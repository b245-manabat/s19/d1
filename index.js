// console.log("Hello Batch 245!");

// What is codtional statements?
	// Conditional Statements allows us to control the flow of our program.
	// It allows us to run statement/instruction of a condition is met or run another separate instruction if not otherwise.

// [SECTION] If, else if and else statement

let numA = -1;

/*
	if statement
		- it will execute the statement/code of blocks if a specified condition is met/true.
*/

if(numA<0) {
	console.log("Hello from numA.");
}

console.log(numA<0);

/*
	Syntax:
	if(condition) {
	statement;
	}
*/
	// The result of the expression added in the if's condition must result to true, else the statement inside the if() will not run.

//  lets reassign the variable numA and run an if statement with the same condition.

numA = 1;

if (numA<0) {
	console.log("Hello from the reqssign value of numA");
}
console.log(numA<0);

// it will not run because the expression now results to false

let city = "New York";

if(city === "New York") {
	console.log("Welcome to "+city+"!");
}

// else if clause
/*
	- Executes a statement if previous conditions are false and if the specified condition is true
	- the "else if" is optional and can be added to capture addition conditions to chagne the flow of a program.
*/

let numH = 1;

if(numH<0) {
	console.log("Hello from if (numH<0).");
}
else if(numH>0) {
	console.log("Hello from else if (numH>0).");
}
/*we were able to run the else if() statement after we evaluated that the if condition was failed/false*/

// If the if() condition was passed and run, we will no longer evaluate to else if() and end the process.

// else if is dependent with if, you cannot use else if clause alone.
/*{
	else if(numH>0) {
		console.log("Hello from (numH>0).");
	}
}*/

// numH = 1;
if(numH !== 1) {
	console.log("Hello from numH !== 1!");
}
else if(numH===1) {
	console.log("Hello from numH === 0!");
}
else if(numH>0) {
	console.log("Hello from the second else if clause!");
}

city = "Tokyo";

if(city === "New York") {
	console.log("Welcome to "+city+"!");
}

else if (city === "Tokyo") {
	console.log("Welcome to "+city+"!");
}

// else statement
	/*
		- Executes a statement if all other conditions are false/not met
		- else if statement is optional and can be added to capture any other result to change the flow of program.
	*/

numH = 2;
// false
if(numH<0) {
	console.log("hello from if statement");
}
// false
else if(numH>2) {
	console.log("hello from first else if statement");
}
// flase
else if(numH>3) {
	console.log("hello from second else if statement");
}
// since all of the conditions above are false/not met else statement will run.
else {
	console.log("hello from the else statement");
}

// Since all of the preceeding if and else condition were not met, the else statement was executed instead
// else statement is also dependent with if statement, it cannot go alone.

/*{
	else {
		console.log("hello from the else inside the code block!");
	}
}*/

	// if, else of and else statement wth fnctions
	/*
		Most of the times we wold like to use if, else if and else statement with functions to control the flow of our application
	*/

let message;

function determineTyphoonIntensity(windSpeed) {
	if(windSpeed<0) {
		return "Invalid argument!";
	}
	else if(windSpeed<=30) {
		return "Not a typhoon yet!";
	}
	else if(windSpeed<=60) {
		return "Tropical depression detected!";
	}
	else if(windSpeed<=88) {
		return "Tropical Storm detected!";
	}
	else if(windSpeed<=117) {
		return "Severe Tropical Storm detected!";
	}
	else if(windSpeed>117) {
		return "Typhoon detected!";
	}
	else {
		return "Invalid argument!";
	}
}
	// mini-activity
	// 1. add return Tropical Storm detected if the windSpeed is between 60 and 89
	// 2. add return Severe Tropical Storm detected if the windSpeed is between 88 and 118
	// 3. if higher that 117, return Typhoon detected!
console.log(determineTyphoonIntensity("q"));

	message = determineTyphoonIntensity(119);

	if(message === "Typhoon detected!") {
		// console.warn() is a good way to print warning in our console that could help us developers act on certain output within our code
		console.warn(message);
	}

// [SECTION] Truthy or falsy
	// JavaScript a "truthy" is a value that is considered true when encountered in Boolean context
	// values are considered true unless defined otherwise.

	// falsy values/ exceptions for truthy
	/*
		1. false
		2. 0
		3. -0
		4. ""
		5. null
		6. undefined
		7. NaN - not a number
	*/

if(true) {
	console.log("true");
}

if(1) {
	console.log("true");
}

if(null) {
	console.log("Hello from null inside the if condition.");
}

else (
	console.log("Hello from the else of null condition.")
)

// [SECTION] Condition operator/Ternary operator
	/*
		the conditional operator
		1. condition/expression
		2. expression to execute if the condition is true of truthy
		3. expression if the condition is falsy

		Syntax:
		(expression) ? iftrue;: ifFalse;
	*/

let ternaryResult = (1>18)? true :false;
console.log(ternaryResult);

// [SECTION] Switch Statement
	/*
		the swith statement evaluates an expression and matches the expression's value to a case class
	*/

	// toLowerCase() method will convert all the letters into small letters
	let day = prompt("What day of the week today?").toLowerCase();

	switch (day) {
		case 'monday':
			console.log("The color of the day is red!");
			// the code will stop here
			break;
		case 'tuesday':
			console.log("The color of the day is orange!");
			break;
		case 'wednesday':
			console.log("The color of the day is yellow!");
			break;
		case 'thursday':
			console.log("The color of the day is green!");
			break;
		case 'friday':
			console.log("The color of the day is blue!");
			break;
		case 'saturday':
			console.log("The color of the day is gray!");
			break;
		case 'sunday':
			console.log("The color of the day is white!");
			break;
		default :
			console.log("Please input a valid day");
			break;
	}

// [SECION] Try-catch-finally
		// try-catch statement for error handling
		// there are instances when the application return an error/warning that is not necessarily an error in the context of our code.
		// these errors are result of an attempt of the programming language to help developers in creating efficient code.

	function showIntensity(windSpeed) {
		try {
			// codes that will be executed or run
			alerat(determineTyphoonIntensity(windSpeed));
			alert("Intensity updated will show alert from try.");
		}
		catch(error) {
			console.warn(error.message);
		}
		finally {
			// continue execution of code regardless of success and failure of code execution
			alert("Intensity updates will show new alert from finally.");
		}
	}

	showIntensity(119);